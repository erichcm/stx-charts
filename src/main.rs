extern crate structopt;
extern crate reqwest;
extern crate walkdir;
extern crate tempdir;
extern crate serde_yaml;

use std::env;
use std::fs::File;
use std::io;
use std::io::copy;
use std::process::Command;
use std::path::Path;
use std::collections::HashMap;

use structopt::StructOpt;
use walkdir::WalkDir;
use tempdir::TempDir;

#[derive(StructOpt, Debug)]
#[structopt(name = "stx-charts",
            about = "A tool to list the used required images in the stx chart.")]
struct Opt {
    #[structopt(short = "u", long = "url", help = "The URL to download the chart file.")]
    url: Option<String>,
    #[structopt(short = "f", long = "file", help = "The path to a local chart file.")]
    file: Option<String>
}

#[derive(Debug)]
enum ChartError {
    RequestError(reqwest::Error),
    FileError(io::Error),
    HTTPError(reqwest::StatusCode),
}

impl From<reqwest::Error> for ChartError {
    fn from(error: reqwest::Error) -> Self {
        ChartError::RequestError(error)
    }
}

impl From<io::Error> for ChartError {
    fn from(error: io::Error) -> Self {
        ChartError::FileError(error)
    }
}

#[derive(Debug, Clone)]
struct Downloader {
    client: reqwest::Client,
}

static HELM_CHART: &'static str = "helm-charts-manifest.tgz";

impl Downloader {
    pub fn new() -> Downloader {
        let proxy_env = env::var("http_proxy").
            or_else(|_| env::var("HTTP_PROXY")).ok();

        if let Some(proxy) = proxy_env {
            let p = reqwest::Proxy::http(&proxy).unwrap();
            Downloader {
                client: reqwest::Client::builder()
                    .proxy(p)
                    .build()
                    .unwrap(),
            }
        } else {
            Downloader {
                client: reqwest::Client::new(),
            }
        }
    }

    fn get(&self, url: &str) -> Result<reqwest::Response, ChartError> {
        let res = self.client.get(url).send()?;
        if res.status().is_success() {
            Ok(res)
        } else {
            Err(ChartError::HTTPError(res.status()))
        }
    }
}


fn download_chart(dest_dir: &TempDir, url: &str) -> Result<(), ChartError> {
    let dl = Downloader::new();
    let mut res = dl.get(url)?;
    let dest_file = dest_dir.path().join(HELM_CHART);
    let mut dest = File::create(dest_file)?;
    copy(&mut res, &mut dest)?;
    Ok(())
}

fn uncompress_file(dir: &TempDir, file: &str) {
    let _ = Command::new("tar")
        .arg("-zxf")
        .arg(file)
        .current_dir(dir)
        .output()
        .expect("Failed to run tar command");
}

// This function is really ugly... :(
fn process_chart(dir: &TempDir) -> Result<(), ChartError> {

    uncompress_file(dir, HELM_CHART);
    let mut images: HashMap<String, i32> = HashMap::new();

    for entry in WalkDir::new(dir)
        .max_depth(2)
        .into_iter()
        .filter_map(|e| e.ok())
    {
        let f_name = entry.file_name().to_string_lossy();
        if f_name.ends_with(".tgz") {
            uncompress_file(dir, &entry.path().to_str().unwrap());
        }
    }

    for entry in WalkDir::new(dir)
        .max_depth(3)
        .into_iter()
        .filter_map(|e| e.ok())
    {
        let f_name = entry.file_name().to_string_lossy();
        if f_name.ends_with("values.yaml") {
            let f = File::open(entry.path())?;
            let r: Result<serde_yaml::Value, _> = serde_yaml::from_reader(f);
            if let Ok(d) = r {
                let images_section = d.get("images");
                if images_section.is_none() {
                    continue;
                }
                let tags = images_section.unwrap().get("tags").unwrap();
                for t in tags.as_mapping().iter() {
                    for (_j, x) in t.iter() {
                        if let serde_yaml::Value::String(a) = x {
                            images.insert(a.to_string(), 0);
                        }
                    }
                }
            }
        }
    }

    for (k, _) in images {
        println!("{}", k);
    }

    Ok(())
}



fn main() {
    let opt = Opt::from_args();
    let mut baseurl: String;
    let tmp_dir = TempDir::new("charts").unwrap();

    if let Some(url) = opt.url {
        baseurl = url;
    } else {
        baseurl = "http://mirror.starlingx.cengn.ca/mirror/starlingx/\
                   master/centos/latest_build/outputs/helm-charts/\
                   helm-charts-stx-openstack-centos-stable-versioned.tgz"
            .to_string();
    }

    if let Some(file) = opt.file {
        let p = Path::new(&file);
        if ! p.exists() {
            println!("File not found: {:?}", p);
            return;
        }
        let dest_file = tmp_dir.path().join(HELM_CHART);
        let mut orig = File::open(p).unwrap();
        let mut dest = File::create(dest_file).unwrap();
        copy(&mut orig, &mut dest).unwrap();
    } else {
        if let Err(e) = download_chart(&tmp_dir, &baseurl) {
            println!("The chart file cannot be downloaded: {:?}", e);
            return;
        }
    }

    if let Err(e) = process_chart(&tmp_dir) {
        println!("Error processing chart: {:?}", e);
    }
}
