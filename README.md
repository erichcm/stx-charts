# stx-charts

A simple tool that lists the required docker images from a set of helm charts.

This tool iterates over all values.yml files inside each chart and get the images
tag to know what are the required docker images.

# Usage

To build this tools run:

```
cargo build --release
```

then run with:

```
./target/release/stx-charts --url <url to chart file>
```

if `url` is not provided the tool will download the latest chart file available.

## Run with docker

Run the image with:

```
docker run erichcm/stx-charts:0.1
```

# Bugs

This prototype code and will have bugs for sure. 
