FROM alpine

COPY target/x86_64-unknown-linux-musl/release/stx-charts /stx-charts

WORKDIR /work

ENTRYPOINT ["/stx-charts"]
